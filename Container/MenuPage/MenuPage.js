import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import fetchMenu, {add} from "../../Components/store/actions/menuAction";
import Menu from "../../Components/Menu/Menu";
import {View} from "react-native";
import FormModal from "../../Components/UI/Modal/Modal";

const MenuPage = () => {
    const dispatch = useDispatch();
    const menu = useSelector(state => state.menu.menu);
    console.log(menu)

    const onPress = (dish) => {
        dispatch(add(dish))
    }

    useEffect(() => {
        dispatch(fetchMenu())
    }, [dispatch]);

    return (
        <View>
            {menu.map(dish => (
                <Menu
                    key={dish.title}
                    image={dish.image}
                    name={dish.title}
                    price={dish.price}
                    add={() => onPress(dish)}
                />
            ))}
            <FormModal/>
        </View>
    );
};

export default MenuPage;