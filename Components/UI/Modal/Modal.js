import React from "react";
import {Modal, Pressable, StyleSheet, Text, View} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import {closeModal, deleteItem, post} from "../../store/actions/menuAction";
import CartItem from "../../CartItem/CartItem";
import {DELIVERY} from "../../../constans";

const FormModal = () => {
    const dispatch = useDispatch();
    const modal = useSelector(state => state.menu.modal);
    const cart = useSelector(state => state.menu.cart);
    const total = useSelector(state => state.menu.price)
    console.log(cart)

    const close = () => {
        dispatch(closeModal());
    };

    const send = (e) => {
        e.preventDefault();
        dispatch(post(cart));
    };

    const removeItem = (dish) => {
        dispatch(deleteItem(dish))
    };


    return (
        <View>
            <Modal
                style={styles.centeredView}
                animationType="slide"
                transparent={true}
                visible={modal}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text style={styles.modalText}>Your order:</Text>
                        {cart.map(item => (
                            <CartItem
                                key={item.title}
                                title={item.title}
                                count={item.amount}
                                price={item.price}
                                removeItem={() => removeItem(item)}
                            />
                        ))}
                        <View style={styles.view}>
                            <Text style={styles.text}>Delivery:</Text>
                            <Text style={styles.text}>{DELIVERY}</Text>
                        </View>
                        <View style={styles.view}>
                            <Text style={styles.text}>Total:</Text>
                            <Text style={styles.text}>{total}</Text>
                        </View>
                        <Pressable
                            style={[styles.button, styles.buttonClose]}
                            onPress={close}
                        >
                            <Text style={styles.textStyle}>Hide Modal</Text>
                        </Pressable>
                        <Pressable
                            style={[styles.button, styles.buttonSend]}
                            onPress={send}
                        >
                            <Text style={styles.textStyle}>Order</Text>
                        </Pressable>
                    </View>
                </View>
            </Modal>
        </View>
    );
};

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22,
        width: 420
    },
    view: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginBottom: 10,
    },
    text: {
        marginRight: 10
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        width: 400,
        height: 800,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    button: {
        borderRadius: 20,
        padding: 10,
        marginBottom: 10,
        elevation: 2
    },
    buttonSend: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 30,
        textAlign: "left",
        fontSize: 40
    }
});

export default FormModal;