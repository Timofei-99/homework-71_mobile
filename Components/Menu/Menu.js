import React from 'react';
import {Image, StyleSheet, Text, TouchableHighlight, View} from "react-native";

const Menu = ({name, price, image, add}) => {
    return (
        <TouchableHighlight onPress={add}>
            <View style={styles.menu}>
                <Image source={{uri: image}}
                       style={styles.img}
                />
                <Text style={styles.text}>{name}</Text>
                <Text style={styles.text}>{price} KGS</Text>
            </View>

        </TouchableHighlight>
    );
};

const styles = StyleSheet.create({
    menu: {
        textAlign: 'center',
        flexDirection: 'row',
        width: 350,
        alignItems: 'center',
        backgroundColor: '#2196F3',
        padding: 10,
        marginBottom: 10,
    },
    img: {
        width: 90,
        height: 60,
    },
    text: {
        padding: 10,
        color: 'white'
    }
})

export default Menu;