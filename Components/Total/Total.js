import React from 'react';
import {View, StyleSheet, Text, Button} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import {openModal} from "../store/actions/menuAction";

const Total = () => {
    const price = useSelector(state => state.menu.price);
    const dispatch = useDispatch();

    const open = () => {
        dispatch(openModal());
    };

    return (
        <View style={styles.total}>
            <Text style={styles.text}>Order Total: {price} KGS</Text>
            <Button onPress={open} title='Checkout'/>
        </View>
    );
};

const styles = StyleSheet.create({
    total: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    text: {
        marginRight: 10
    }
});

export default Total;