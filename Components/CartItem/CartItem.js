import React from 'react';
import {Pressable, StyleSheet, Text, View} from "react-native";
import {useDispatch} from "react-redux";

const CartItem = ({title, count, price, removeItem}) => {
    const dispatch = useDispatch();





    return (
        <View style={styles.view}>
            <Text style={styles.text}>{title}</Text>
            <Text style={styles.text}>x{count}</Text>
            <Text style={styles.text}>{price} KGS</Text>
            <Pressable
                style={[styles.button, styles.buttonSend]}
            >
                <Text onPress={removeItem} style={styles.textStyle}>Delete</Text>
            </Pressable>
        </View>
    );
};

const styles = StyleSheet.create({
    view: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginBottom: 10,
    },
    button: {
        borderRadius: 20,
        padding: 10,
        marginBottom: 10,
        elevation: 2
    },
    buttonSend: {
        backgroundColor: "#F194FF",
    },
    text: {
        marginRight: 10
    }
})

export default CartItem;