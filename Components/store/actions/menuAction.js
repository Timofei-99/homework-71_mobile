import axiosAPI from "../../../axiosAPI";

export const FETCH_MENU_REQUEST = 'FETCH_MENU_REQUEST';
export const FETCH_MENU_SUCCESS = 'FETCH_MENU_SUCCESS';
export const FETCH_MENU_FAILURE = 'FETCH_MENU_FAILURE';

export const POST_ORDER_REQUEST = 'POST_ORDER_REQUEST';
export const POST_ORDER_SUCCESS = 'POST_ORDER_SUCCESS';
export const POST_ORDER_FAILURE = 'POST_ORDER_FAILURE';

export const DELETE_ORDER_ITEM = 'DELETE_ORDER_ITEM';

export const ADD_DISH = 'ADD_DISH';

export const OPEN_MODAL = 'OPEN_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';

export const openModal = () => ({type: OPEN_MODAL});
export const closeModal = () => ({type: CLOSE_MODAL});

export const addDish = (dish, price) => ({type: ADD_DISH, payload: dish, price});
export const deleteDish = (item, price) => ({type: DELETE_ORDER_ITEM, payload: item, price})

export const postOrderRequest = () => ({type: POST_ORDER_REQUEST});
export const postOrderSuccess = () => ({type: POST_ORDER_SUCCESS});
export const postOrderFailure = () => ({type: POST_ORDER_FAILURE});

export const post = (order) => {
    return async (dispatch, getState) => {
        try {
            dispatch(postOrderRequest());

            await axiosAPI.post('order.json', order);
            dispatch(postOrderSuccess());
        } catch (error) {
            dispatch(postOrderFailure(error));
        }
    }
}

export const deleteItem = (dish) => {
    return (dispatch, getState) => {
        const copy = [...getState().menu.cart];
        const index = copy.findIndex(item => item.title === dish.title);
        if (index === -1) {
            copy.push({title: dish.title, price: dish.price, amount: 1, id: dish.id});
        } else if (copy[index].amount <= 1) {
            copy.splice(index, 1);
        } else {
            copy[index].amount--;
        }
        dispatch(deleteDish(copy, dish.price))
    }
}

export const add = (dish) => {
    return (dispatch, getState) => {
        const copy = [...getState().menu.cart];
        const index = copy.findIndex(item => item.title === dish.title);
        if (index === -1) {
            copy.push({title: dish.title, price: dish.price, amount: 1, id: dish.id});
        } else {
            copy[index].amount++;
        }
        dispatch(addDish(copy, dish.price))
    }
}

export const fetchMenuRequest = () => ({type: FETCH_MENU_REQUEST});
export const fetchMenuSuccess = (dish) => ({type: FETCH_MENU_SUCCESS, payload: dish});
export const fetchMenuFailure = () => ({type: FETCH_MENU_FAILURE});

const fetchMenu = () => {
    return async (dispatch) => {
        try {
            dispatch(fetchMenuRequest());
            const response = await axiosAPI.get('menu.json');
            const dish = Object.keys(response.data).map(dish => ({
                ...response.data[dish],
                id: dish
            }));
            dispatch(fetchMenuSuccess(dish));
        } catch (error) {
            dispatch(fetchMenuFailure(error));
        }
    }
}

export default fetchMenu;