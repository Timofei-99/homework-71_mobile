import {
    ADD_DISH,
    CLOSE_MODAL, DELETE_ORDER_ITEM,
    FETCH_MENU_FAILURE,
    FETCH_MENU_REQUEST,
    FETCH_MENU_SUCCESS,
    OPEN_MODAL, POST_ORDER_FAILURE, POST_ORDER_REQUEST, POST_ORDER_SUCCESS
} from "../actions/menuAction";
import {DELIVERY} from "../../../constans";

const initialState = {
    menu: [],
    price: 0,
    cart: [],
    modal: false,
};

const menuReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_MENU_REQUEST:
            return {...state}
        case FETCH_MENU_SUCCESS:
            return {...state, menu: action.payload}
        case FETCH_MENU_FAILURE:
            return {...state}
        case POST_ORDER_REQUEST:
            return {...state}
        case POST_ORDER_SUCCESS:
            return {...state, modal: false, cart: [], price: 0}
        case POST_ORDER_FAILURE:
            return {...state}
        case ADD_DISH:
            return {...state,
                cart: action.payload,
                price: state.price + parseInt(action.price),
            }
        case DELETE_ORDER_ITEM:
            return {...state,
                cart: action.payload,
                price: state.price - parseInt(action.price),
            }
        case OPEN_MODAL:
            return {...state, modal: true, price: state.price + DELIVERY}
        case CLOSE_MODAL:
            return {...state, modal: false, price: state.price - DELIVERY}
        default:
            return state;
    }
}

export default menuReducer;