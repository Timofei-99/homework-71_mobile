import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import React from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import menuReducer from "./Components/store/reducers/menuReducer";
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import Menu from "./Components/Menu/Menu";
import MenuPage from "./Container/MenuPage/MenuPage";
import Total from "./Components/Total/Total";

export default function App() {
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    const rootReducer = combineReducers({
        menu: menuReducer,
    });

    const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

    return (
        <Provider store={store}>
            <ScrollView>
                <View style={styles.container}>
                    <Text style={styles.text}>Turtle Pizza</Text>
                    <MenuPage/>
                </View>
            </ScrollView>
            <View style={styles.check}>
                <Total/>
            </View>
        </Provider>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 60,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    text: {
        fontSize: 40,
        padding: 5,
        marginBottom: 20
    },
    check: {
        flex: 1,
        marginBottom: 40,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'flex-end',
    }
});
